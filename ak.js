criterion.forEach(function(item) {
    item.maprange = item.startmap + " ";
    
    if (item.endmap != null && item.startmap !== item.endmap) {
        item.maprange += " - " + item.endmap;
    }
})
